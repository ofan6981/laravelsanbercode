@extends('layouts.master')
@section('title')
Halaman Biodata
@endsection
@section('content')
<h1>Create New Account</h1>

<h2>Sign Up Form</h2>
<form action="/home" method="POST">
    @csrf
    <label>First Name</label><br><input type="text" name="fname"><br><br>
    <label>Last Name</label><br><input type="text" name="lname"><br><br>
    <label>Gender</label><br><input type="radio">Man<br><input type="radio">Woman<br><input type="radio">Other<br><br>
    <label>Nationality</label>
    <select name="nation">
        <option value="">Indonesia</option>
        <option value="">Singapore</option>
        <option value="">Malaysia</option>
        <option value="">Australia</option>
    </select>
    <br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Arabic<br>
    <input type="checkbox">Japanese<br><br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>

    <input type="submit" value="Kirim">
</form>
@endsection