@extends('layouts.master')
@section('title')
Halaman Tampil Cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-sm btn-primary">Tambah Cast</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Act</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->nama}}</td>

            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail Data</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit Data</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>


        </tr>

        @empty
        <tr>
            <td>tidak ada cast</td>
        </tr>

        @endforelse
    </tbody>
</table>

@endsection