@extends('layouts.master')
@section('title')
Halaman Edit Cast
@endsection
@section('content')

<form action="/cast/{{$castData->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" value="{{$castData->nama}}" class="form-control @error('nama') is-invalid @enderror" name="nama" placeholder="Nama">
        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur</label>
        <input type="text" value="{{$castData->umur}}" class="form-control @error('umur') is-invalid @enderror" name="umur" placeholder="Umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control @error('umur') is-invalid @enderror" id="" cols="30" rows="10">{{$castData->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection