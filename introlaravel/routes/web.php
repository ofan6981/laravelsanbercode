<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'dashboard']);
Route::get('/daftar', [BiodataController::class, 'bio']);
Route::post('/home', [BiodataController::class, 'kirim']);

// Route::get('/master', function () {
//     return view('layouts.master');
// });
Route::get('/data-table', function () {
    return view('page.data-table');
});

Route::get('/table', function () {
    return view('page.table');
});

// crud cast
//c:create (route mengarah ke hal tambah cast) 
Route::get('/cast/create', [CastController::class, 'create']);

// utuk menyimpan data ke DB & validasi
Route::post('/cast', [CastController::class, 'store']);

// r: read data cast / fetch semua data
Route::get('/cast', [CastController::class, 'index']);

// r: read data cast detail by id/ fetch detail data
Route::get('/cast/{id}', [CastController::class, 'show']);

// u : update data cast, mengarah ke hal edit
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// u : route untuk update data
Route::put('/cast/{id}', [CastController::class, 'update']);

// d : delete data cast
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
