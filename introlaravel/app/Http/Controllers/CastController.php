<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        // ini untuk validasi inputan form
        $request->validate([
            'nama' => 'required|min:4',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // ini untuk insert
        DB::table('cast')->insert([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);

        // redirect ke hal cast

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.tampil', ['cast' => $cast]);
        // return view('cast.tampil', compact('cast')); //compact digunakan untuk mengirim data ke view, perhatiakn bagian -cast.tampil- (cast nama folder, tampil itu filenya buat halnya)
    }

    public function show($id)
    {
        $castData = DB::table('cast')->find($id);
        return view('cast.detail', ['castData' => $castData]);
    }

    public function edit($id)
    {
        $castData = DB::table('cast')->find($id);
        return view('cast.edit', ['castData' => $castData]);
    }

    public function update($id, Request $request)
    {
        // ini untuk validasi inputan form
        $request->validate([
            'nama' => 'required|min:4',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //upatate data
        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request->input('nama'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio')
                ]
            );
        return redirect('/cast');
    }

    //delete data

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
