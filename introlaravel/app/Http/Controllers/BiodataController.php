<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class BiodataController extends Controller
{
    public function bio()
    {
        return view('page.biodata');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view('page.dashboard', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
